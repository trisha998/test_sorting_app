package com.gdzidziguri.sorting;
import com.gdzidziguri.util.Sort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingTenElementsCaseTest {

    private final int[] arr;
    private final int[] res;

    public SortingTenElementsCaseTest(int[] arr, int[] res) {
        this.arr = arr;
        this.res = res;
    }

    @Parameterized.Parameters
    public static List<Object[]> data() {
        return Arrays.asList(new int[][][] {
                { { 4, 12, 0, 9, -5, 8, -2, 11, 100, 4 }, { -5, -2, 0,4, 4,8,9,11,12,100, } },
                { { 20, 19, 18, 17, 16, 15, 14, 13, 12, 11 }, { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 } },
                { { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2  }, { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 } }
        });
    }

    @Test
    public void testTenElementsCase() {

        Sort.bubbleSort(arr);
        assertArrayEquals(res, arr);


    }

}
