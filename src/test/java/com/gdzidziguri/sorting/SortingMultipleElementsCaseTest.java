package com.gdzidziguri.sorting;
import com.gdzidziguri.util.Sort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingMultipleElementsCaseTest {

    private final int[] arr;
    private final int[] result;

    public SortingMultipleElementsCaseTest(int[] arr, int[] res) {
        this.arr = arr;
        this.result = res;
    }

    @Parameterized.Parameters
    public static List<Object[]> data() {
        return Arrays.asList(new int[][][] {
                { { 9, 4 }, { 4, 9 } },
                { {-6, 4, 7, 14, -14, 18, 16}, { -14, -6, 4, 7, 14, 16, 18 } },
                { { 22, 21, 20, 19, 18, 17, 16, 15,14,13 }, { 13,14,15,16,17,18,19,20,21,22} },
                { { 5, 5, 5, 5, 5, 5, 5,  }, { 5, 5, 5, 5, 5, 5, 5 } }
        });
    }

    @Test
    public void testMultipleElementsCase() {

        Sort.bubbleSort(arr);

        assertArrayEquals(result, arr);


    }

}
