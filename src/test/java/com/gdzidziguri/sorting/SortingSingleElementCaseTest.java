package com.gdzidziguri.sorting;

import com.gdzidziguri.util.Sort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingSingleElementCaseTest {

    private final int[] arr;
    private final int[] result;

    public SortingSingleElementCaseTest(int[] arr, int[] res) {
        this.arr = arr;
        this.result = res;
    }

    @Parameterized.Parameters
    public static List<Object[]> data() {
        return Arrays.asList(
                new int[][][] {
                        { { 4 }, { 4 } },
                        { { 6 }, { 6 } },
                        { { 7 }, { 7 } },
                });
    }

    @Test
    public void testSingleElementCase() {
        Sort.bubbleSort(arr);

        assertArrayEquals(result, arr);
    }

}
