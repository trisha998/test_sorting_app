package com.gdzidziguri.sorting;
import com.gdzidziguri.util.Sort;
import org.junit.Test;
import static org.junit.Assert.assertArrayEquals;

public class SortingZeroElementsCaseTest {

    @Test
    public void testZeroElementsCase() {
        int[] arr = {};
        int[] res = {};

        Sort.bubbleSort(arr);

        assertArrayEquals(res, arr);
    }
}
