package com.gdzidziguri.sorting;

import com.gdzidziguri.util.Sort;
import org.junit.Test;

public class SortingNullCaseTest {

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase() {
        Sort.bubbleSort(null);
    }

}
