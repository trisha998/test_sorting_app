package com.gdzidziguri;
import com.gdzidziguri.util.Sort;
import org.apache.commons.cli.*;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        int[] array = parseIntegers(args);
        Sort.bubbleSort(array);

        System.out.println(Arrays.toString(array));
    }
    public static int[] parseIntegers(String[] args) {
        Options options = new Options();
        Option option = new Option("num", "list", true, "Numbers to be sorted");
        option.setArgs(10);
        options.addOption(option);


        CommandLineParser commandLineParser = new BasicParser();
        CommandLine command = null;
        try {
            command = commandLineParser.parse(options, args);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (command != null)
            if (command.hasOption("num")) {
                String[] array = command.getOptionValues("num");
                return Arrays.stream(array).mapToInt(Integer::parseInt).toArray();
            }

        return null;
    }


}
